import '../vendor/pace/pace.js';
import '../vendor/pace/pace-hideBody.css';
import '../vendor/pace/pace-theme.css';
Pace.start();

import {
    enable as enableDarkMode,
    disable as disableDarkMode,
} from 'darkreader';
import Cookies from 'js-cookie';

import '../scss/public.scss';
import '../vendor/materialize/js/bin/materialize.js';
// import '../../node_modules/bootstrap/scss/bootstrap.scss';
import '../vendor/material-bootstrap/assets/scss/material-kit.scss';
import '../vendor/material-bootstrap/assets/js/bootstrap-material-design.js';
import '../vendor/material-bootstrap/assets/js/plugins/moment.min.js';
import '../vendor/material-bootstrap/assets/js/plugins/bootstrap-selectpicker.js';
import '../vendor/material-bootstrap/assets/js/plugins/bootstrap-tagsinput.js';
import '../vendor/material-bootstrap/assets/js/plugins/jasny-bootstrap.min.js';
import '../vendor/material-bootstrap/assets/js/plugins/jquery.flexisel.js';
import '../vendor/material-bootstrap/assets/js/plugins/bootstrap-datetimepicker.min.js';
import '../vendor/material-bootstrap/assets/js/plugins/nouislider.min.js';
import '../vendor/material-bootstrap/assets/js/material-kit.js?v=2.0.0';
import '../vendor/font-mfizz-2.4.1/font-mfizz.css';

import '../css/public.css';

document.addEventListener('DOMContentLoaded', function() {
  M.Sidenav.init(document.querySelectorAll('.sidenav'));
  M.FloatingActionButton.init(document.querySelectorAll('.fixed-action-btn'));
  M.Tooltip.init(document.querySelectorAll('.tooltipped'));
  M.TapTarget.init(document.querySelectorAll('.tap-target'));

  window.M = M; //expose materialize JS to client

  if (Cookies.get("menuTapOpened") !== "1") {
    M.TapTarget.getInstance(document.getElementById("menuTapTarget")).open();
    Cookies.set("menuTapOpened", "1", { expires: 30 });
  }

  //twemoji
  twemoji.parse(document.body);

  //dark mode
  var darkModeOptions = {
    brightness: 150,
    contrast: 80,
    sepia: 0,
  };
  var button = document.createElement("Button");
  if (Cookies.get("darkModeToggle") === "1") {
    enableDarkMode(darkModeOptions);
    button.innerHTML = "Light Mode";
  } else {
    button.innerHTML = "Dark Mode";
  }
  button.style = "top:0;right:0;position:absolute;z-index: 9999";
  button.addEventListener("click", function (e) {
    if (Cookies.get("darkModeToggle") === "1") {
      disableDarkMode();
      Cookies.set("darkModeToggle", "0", { expires: 30 });
      this.innerHTML = "Dark Mode";
    } else {
      enableDarkMode(darkModeOptions);
      Cookies.set("darkModeToggle", "1", { expires: 30 });
      this.innerHTML = "Light Mode";
    }
  });
  document.body.appendChild(button);
});
