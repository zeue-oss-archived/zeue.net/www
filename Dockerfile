FROM php:7.3-alpine

ARG MONGO_URL
ENV MONGO_URL ${MONGO_URL}

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/main' >> /etc/apk/repositories
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/community' >> /etc/apk/repositories

RUN apk add --update --no-cache libressl-dev libzip-dev util-linux composer zip libpng-dev git mongodb alpine-sdk autoconf python nodejs yarn npm && \
        docker-php-ext-install pcntl gd zip

RUN pecl install mongodb

RUN docker-php-ext-enable mongodb

RUN mkdir /www.zeue.net/

COPY . /www.zeue.net/

RUN cd /www.zeue.net && \
      cp .env.prod .env && \
      composer install && \
      npm i && \
      npm run build

CMD php /www.zeue.net/bin/console server:run *:$PORT
