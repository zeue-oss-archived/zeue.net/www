<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;

class APIController extends AbstractController
{
    /**
     * @Route("/api/blog/posts")
     */
    public function getBlogPosts()
    {
        try {
          $blogPosts = $this->forward('App\Controller\MongoController::findExact',
          [
            "database" => "zeue-net",
            "collection" => "blog",
            "findCriteria" =>
            [
              "meta.status" => "public"
            ]
          ])->getContent();

          $returnArr = (json_decode($blogPosts, true))["message"];

          return new JsonResponse(["success" => 1, "message" => $returnArr], Response::HTTP_OK);
        } catch (\Exception $e) {
          return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/api/blog/post/{post_id}"), methods={"GET"}
     */
    public function getSingleBlogPost(string $post_id)
    {
        try {
          $blogPost = $this->forward(
            'App\Controller\MongoController::findOneById',
            [
              "database" => "zeue-net",
              "collection" => "blog",
              "objectId" => $post_id
            ]
          )->getContent();

          $returnArr = (json_decode($blogPost, true))["message"];

          return new JsonResponse(["success" => 1, "message" => $returnArr], Response::HTTP_OK);
        } catch (\Exception $e) {
          return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/api/products")
     */
    public function products()
    {
        try {
          $products = Yaml::parseFile('../config/zeue/products.yaml');

          if ($products) {
            return new JsonResponse(["success" => 1, "message" => $products], Response::HTTP_OK);
          } else {
            return new JsonResponse(["success" => 0, "message" => "no products found!"], Response::HTTP_OK);
          }

          return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
          return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/api/product/{product_id}"), methods={"GET"}
     */
    public function product(string $product_id)
    {
        try {
          $products = Yaml::parseFile('../config/zeue/products.yaml');

          if (array_key_exists($product_id, $products)) {
            return new JsonResponse(["success" => 1, "message" => $products[$product_id]], Response::HTTP_OK);
          } else {
            return new JsonResponse(["success" => 0, "message" => "product ID not found!"], Response::HTTP_OK);
          }

          return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
          return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
        }
    }
}
