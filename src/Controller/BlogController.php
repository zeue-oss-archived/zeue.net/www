<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index()
    {
      $apiResponse = $this->forward('App\Controller\APIController::getBlogPosts')->getContent();

      $blogPosts = json_decode($apiResponse, true)["message"];

      return $this->render('blog/index.html.twig', [
        'blogPosts' => $blogPosts
      ]);
    }
}
