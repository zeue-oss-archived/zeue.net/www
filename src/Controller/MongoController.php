<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class MongoController extends AbstractController
{
  private function getMongoClient()
  {
      return new \MongoDB\Client(getenv('MONGO_URL'));
  }

  public function findExact($database = null, $collection = null, $findCriteria = null)
  {
    if ($database && $collection && $findCriteria) {
      try {
        $cursor = ($this->getMongoClient())->$database->$collection->find($findCriteria);

        $returnArr = [];

        foreach ($cursor as $_x) {
            $returnArr[] = (array)$_x;
        }

        return new JsonResponse(["success" => "1", "message" => $returnArr], Response::HTTP_OK);
      } catch (\Exception $e) {
        return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
      }
    } else {
      return new JsonResponse(["success" => 0, "message" => "missing parameters"], Response::HTTP_BAD_REQUEST);
    }
    return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
  }

  public function findOneById($database = null, $collection = null, $objectId = null)
  {
    if ($database && $collection && $objectId) {
      try {
        $document = ($this->getMongoClient())->$database->$collection->findOne(["_id" => new \MongoDB\BSON\ObjectId($objectId)]);

        return new JsonResponse(["success" => "1", "message" => $document], Response::HTTP_OK);
      } catch (\Exception $e) {
        return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
      }
    } else {
      return new JsonResponse(["success" => 0, "message" => "missing parameters"], Response::HTTP_BAD_REQUEST);
    }
    return new JsonResponse(["success" => 0, "message" => "generic error ".__METHOD__."#".__LINE__], Response::HTTP_BAD_REQUEST);
  }
}
