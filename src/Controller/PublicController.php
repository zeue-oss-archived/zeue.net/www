<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
      return $this->redirectToRoute('about');
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
      return $this->render('public/about.html.twig');
    }

    /**
     * @Route("/hosting", name="hosting")
     */
    public function hosting()
    {
      return $this->render('public/hosting.html.twig');
    }

    /**
     * @Route("/hosting/websites", name="hosting/websites")
     */
    public function hosting_websites()
    {
      return $this->render('public/hosting_websites.html.twig');
    }

    /**
     * @Route("/hosting/gaming", name="hosting/gaming")
     */
    public function hosting_gaming()
    {
      return $this->render('public/hosting_gaming.html.twig');
    }

    /**
     * @Route("/pay", name="pay")
     */
    public function pay()
    {
      if (isset($_GET["p"])) {
        \Stripe\Stripe::setApiKey(getenv("STRIPE_API_KEY"));

        if ($_GET["p"] == "OTHER") {
          $priceFix = preg_replace('/[.,]/', '', $_GET["cp"]);

          $session = \Stripe\Checkout\Session::create([
            "success_url" => "https://zeue.net/pay?s=1",
            "cancel_url" => "https://zeue.net/pay?s=0",
            "payment_method_types" => ["card"],
            "mode" => "payment",
            "line_items" => [[
              "name" => "Custom Order",
              "description" => $_GET["cd"],
              "amount" => $priceFix,
              "currency" => "gbp",
              "quantity" => 1
            ]]
          ]);
        } else {
          $product = json_decode($this->forward(
            'App\Controller\APIController::product',
            [
              "product_id" => $_GET["p"]
            ]
          )->getContent(), true)["message"];

          // https://stripe.com/docs/api/checkout/sessions/create
          if ($product["type"] === "subscription") {
            $session = \Stripe\Checkout\Session::create([
              "success_url" => "https://zeue.net/pay?s=1",
              "cancel_url" => "https://zeue.net/pay?s=0",
              "payment_method_types" => ["card"],
              "mode" => $product["type"],
              "subscription_data" => [
                "items" => [[
                  "plan" => $_GET["p"]."_".$_GET["l"]
                ]]
              ]
            ]);
          } else {
            $session = \Stripe\Checkout\Session::create([
              "success_url" => "https://zeue.net/pay?s=1",
              "cancel_url" => "https://zeue.net/pay?s=0",
              "payment_method_types" => ["card"],
              "mode" => $product["type"],
              "line_items" => [[
                "name" => $product["name"],
                "description" => $product["description"],
                "amount" => $product["price"],
                "currency" => $product["currency"],
                "quantity" => 1
              ]]
            ]);
          }
        }

        return $this->render('public/pay.html.twig', [
          "interim" => true,
          "CHECKOUT_SESSION_ID" => $session["id"]
        ]);
      } else {
        $products = json_decode($this->forward(
          'App\Controller\APIController::products'
        )->getContent(), true)["message"];

        return $this->render('public/pay.html.twig', [
          "products" => $products
        ]);
      }
    }
}
