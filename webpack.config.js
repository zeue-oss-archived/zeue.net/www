var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .setManifestKeyPrefix('build/')
    .addEntry('global', './assets/js/global.js')
    .addEntry('jquery', './assets/js/jquery.js')
    .addEntry('public', './assets/js/public.js')
    .addEntry('blog', './assets/js/blog.js')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    //.autoProvidejQuery()
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .enableSassLoader()
    //DOESN'T WORK WITH CURRENT CLOUDFLARE CONFIG
    //.enableIntegrityHashes(Encore.isProduction())
    .copyFiles([
      {
        from: "./assets/img",
        to: 'images/[path][name].[ext]'
      },
      {
        from: "./assets/vid",
        to: 'videos/[path][name].[ext]'
      }
    ])
;

let config = Encore.getWebpackConfig();

config.module.rules.push({
  test: /\.(png|jpe?g|gif|ttf|eot|svg|otf|cur|webm)$/i,
  use: ['file-loader']
});

module.exports = config;
